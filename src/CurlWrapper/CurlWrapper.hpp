#pragma once

#include <curl/curl.h>

#include <string>

namespace curl
{
class CurlWrapper
{
  public:
    CurlWrapper();
    ~CurlWrapper() = default;

    bool downloadFile(std::string url, std::string outFile);

  private:
};
}  // namespace curl