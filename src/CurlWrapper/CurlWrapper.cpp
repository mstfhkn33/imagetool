#include "CurlWrapper.hpp"

#include <stdio.h>

#include <iostream>

namespace curl
{
size_t writeData(void* ptr, size_t size, size_t nmemb, FILE* stream)
{
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}

CurlWrapper::CurlWrapper() {}

bool CurlWrapper::downloadFile(std::string url, std::string outFile)
{
    FILE* fp;
    CURLcode res;
    CURL* curl = curl_easy_init();
    if (curl)
    {
        fp = fopen(outFile.c_str(), "wb");
        if (fp == NULL)
        {
            std::cout << "File " << outFile << " could not open!" << std::endl;
            return false;
        }
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeData);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);
        if (res != CURLE_OK)
        {
            std::cout << "Curl ERROR: " << res << "!" << std::endl;
            curl_easy_cleanup(curl);
            fclose(fp);
            return false;
        }
        curl_easy_cleanup(curl);
        fclose(fp);
    }
    else
    {
        std::cout << "Curl initialization is failed!" << std::endl;
        return false;
    }
    return true;
}

}  // namespace curl