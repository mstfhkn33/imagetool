#include "URLParser.hpp"

namespace parser
{

URLParser::URLParser(std::string url) : url(url)
{
    parse();
}

void URLParser::print()
{
    std::cout << "URL Parser results:" << std::endl;
    std::cout << "url: " << url << std::endl;
    std::cout << "mainTag: " << mainTag << std::endl;
    std::cout << "operation: " << operation << std::endl;
    std::cout << "operationValue: " << operationValue << std::endl;
    std::cout << "imageUrl: " << imageUrl << std::endl;
}

void URLParser::parse()
{
    getMainTag();
    getOperationPart();
    getImageUrlPart();
}

void URLParser::getMainTag()
{
    mainTagPart = getNextPart(url, 0);
    // remove first and last / characters
    mainTag = mainTagPart.substr(1, mainTagPart.length() - 2);
}

void URLParser::getOperationPart()
{
    operationPart = getNextPart(url, mainTagPart.length());
    if (operationPart.find(":") == std::string::npos)
    {
        operation = operationPart.substr(0, operationPart.length() - 1);
        operationValue = "";
    }
    else
    {
        auto opPair = parseKeyValue(operationPart);
        operation = opPair.first;
        operationValue = opPair.second.substr(0, opPair.second.length() - 1);
    }
}

void URLParser::getImageUrlPart()
{
    imageUrlPart = url.substr(mainTagPart.length() + operationPart.length(), url.length());
    auto imagePair = parseKeyValue(imageUrlPart);
    imageUrl = imagePair.second.substr(0, imagePair.second.length());
    if (imageUrl.rfind("https://", 0) != 0)
    {
        imageUrl = "https://" + imageUrl;
    }
}

std::string URLParser::getNextPart(std::string url, size_t pos)
{
    size_t endPos = url.find("/", pos + 1);
    return url.substr(pos, (endPos - pos) + 1);
}

std::pair<std::string, std::string> URLParser::parseKeyValue(std::string token)
{
    size_t pos = token.find(":");
    std::pair<std::string, std::string> returnVal(token.substr(0, pos), token.substr(pos + 1, token.length()));
    return returnVal;
}

}  // namespace parser