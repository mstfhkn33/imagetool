#pragma once

#include <iostream>
#include <string>

namespace parser
{
class URLParser
{
  public:
    URLParser(std::string url);
    ~URLParser() = default;

    inline std::string getMainTag() const { return mainTag; }
    inline std::string getOperation() const { return operation; }
    inline std::string getOperationValue() const { return operationValue; }
    inline std::string getTargetImageUrl() const { return imageUrl; }

    void print();

  private:
    std::string url;
    std::string mainTagPart;
    std::string mainTag;
    std::string operationPart;
    std::string operation;
    std::string operationValue;
    std::string imageUrlPart;
    std::string imageUrl;

    void parse();
    void getMainTag();
    void getOperationPart();
    void getImageUrlPart();
    std::string getNextPart(std::string url, size_t pos);
    std::pair<std::string, std::string> parseKeyValue(std::string token);
};
}  // namespace parser