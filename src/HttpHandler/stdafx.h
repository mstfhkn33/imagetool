#ifndef STDAFX_H_INCLUDED
#define STDAFX_H_INCLUDED

#pragma once

#include <algorithm>
#include <fstream>
#include <iostream>
#include <random>
#include <sstream>
#include <string>
#include <vector>

#ifdef _WIN32
#define NOMINMAX
#include <Windows.h>
#else
#include <sys/time.h>
#endif

#include "cpprest/asyncrt_utils.h"
#include "cpprest/containerstream.h"
#include "cpprest/filestream.h"
#include "cpprest/http_listener.h"
#include "cpprest/json.h"
#include "cpprest/producerconsumerstream.h"
#include "cpprest/uri.h"

#pragma warning(push)
#pragma warning(disable : 4457)
#pragma warning(pop)
#include <ctime>
#include <locale>
#endif  // STDAFX_H_INCLUDED