#pragma once

#include <iostream>

#include "stdafx.h"

namespace http_handler
{
class HttpHandler
{
  public:
    HttpHandler();
    HttpHandler(utility::string_t url);
    ~HttpHandler() = default;

    pplx::task<void> open() { return httpListener.open(); }
    pplx::task<void> close() { return httpListener.close(); }

  private:
    static const std::string DownloadFileName;
    static const std::string OutputFileName;
    static const std::string OperationResize;
    static const std::string OperationRotate;
    static const std::string OperationCrop;
    static const std::string OperationGrayScale;

    void httpGetHandler(web::http::http_request message);
    void httpPutHandler(web::http::http_request message);
    void httpPostHandler(web::http::http_request message);
    void httpDeleteHandler(web::http::http_request message);
    static void httpErrorHandler(pplx::task<void>& t);
    web::http::experimental::listener::http_listener httpListener;
};
}  // namespace http_handler