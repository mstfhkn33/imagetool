#include "HttpHandler.hpp"

#include "CurlWrapper.hpp"
#include "IMagickWrapper.hpp"
#include "URLParser.hpp"

namespace http_handler
{
const std::string HttpHandler::DownloadFileName = "download.jpg";
const std::string HttpHandler::OutputFileName = "output.jpg";
const std::string HttpHandler::OperationResize = "resize";
const std::string HttpHandler::OperationRotate = "rotate";
const std::string HttpHandler::OperationCrop = "crop";
const std::string HttpHandler::OperationGrayScale = "grayscale";

HttpHandler::HttpHandler() {}
HttpHandler::HttpHandler(utility::string_t url) : httpListener(url)
{
    httpListener.support(web::http::methods::GET, std::bind(&HttpHandler::httpGetHandler, this, std::placeholders::_1));
    httpListener.support(web::http::methods::PUT, std::bind(&HttpHandler::httpPutHandler, this, std::placeholders::_1));
    httpListener.support(web::http::methods::POST,
                         std::bind(&HttpHandler::httpPostHandler, this, std::placeholders::_1));
    httpListener.support(web::http::methods::DEL,
                         std::bind(&HttpHandler::httpDeleteHandler, this, std::placeholders::_1));
}

void HttpHandler::httpGetHandler(web::http::http_request message)
{
    // ucout << message.relative_uri().to_string() << std::endl;
    // ucout << message.to_string() << std::endl;
    std::cout << "GET Request received!" << std::endl;

    parser::URLParser urlParser(message.relative_uri().to_string());
    urlParser.print();

    curl::CurlWrapper curlWrapper;
    bool res = curlWrapper.downloadFile(urlParser.getTargetImageUrl(), DownloadFileName);
    if (!res)
    {
        message.reply(web::http::status_codes::InternalError, "File download is failed!");
        return;
    }

    image_magick::IMagickWrapper imageMagickWrapper(DownloadFileName);
    if (urlParser.getOperation().compare(OperationResize) == 0)
    {
        res = imageMagickWrapper.resizeImage(urlParser.getOperationValue(), OutputFileName);
    }
    else if (urlParser.getOperation().compare(OperationRotate) == 0)
    {
        res = imageMagickWrapper.rotateImage(urlParser.getOperationValue(), OutputFileName);
    }
    else if (urlParser.getOperation().compare(OperationCrop) == 0)
    {
        res = imageMagickWrapper.cropImage(urlParser.getOperationValue(), OutputFileName);
    }
    else if (urlParser.getOperation().compare(OperationGrayScale) == 0)
    {
        res = imageMagickWrapper.grayScaleImage(OutputFileName);
    }
    else
    {
        message.reply(web::http::status_codes::InternalError, "Wrong operation!");
        return;
    }
    if (!res)
    {
        message.reply(web::http::status_codes::InternalError, "Resizing the image is failed!");
        return;
    }

    concurrency::streams::fstream::open_istream(OutputFileName, std::ios::in)
        .then([=](concurrency::streams::istream is)
              { message.reply(web::http::status_codes::OK, is).then([](pplx::task<void> t) { httpErrorHandler(t); }); })
        .then(
            [=](pplx::task<void> t)
            {
                try
                {
                    t.get();
                }
                catch (...)
                {
                    // opening the file (open_istream) failed.
                    // Reply with an error.
                    message.reply(web::http::status_codes::InternalError)
                        .then([](pplx::task<void> t) { httpErrorHandler(t); });
                }
            });
}

void HttpHandler::httpPutHandler(web::http::http_request message)
{
    ucout << message.to_string() << std::endl;
    std::string rep = U("WRITE YOUR OWN PUT OPERATION");
    message.reply(web::http::status_codes::OK, rep);
}

void HttpHandler::httpPostHandler(web::http::http_request message)
{
    ucout << message.to_string() << std::endl;

    message.reply(web::http::status_codes::OK, message.to_string());
}

void HttpHandler::httpDeleteHandler(web::http::http_request message)
{
    ucout << message.to_string() << std::endl;

    std::string rep = U("WRITE YOUR OWN DELETE OPERATION");
    message.reply(web::http::status_codes::OK, rep);
}

void HttpHandler::httpErrorHandler(pplx::task<void>& t)
{
    try
    {
        t.get();
    }
    catch (std::exception& e)
    {
        std::cout << "Exception message: " << e.what() << std::endl;
    }
}
}  // namespace http_handler