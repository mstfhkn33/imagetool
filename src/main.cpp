#include <Magick++.h>

#include <iostream>

#include "HttpHandler.hpp"

std::unique_ptr<http_handler::HttpHandler> g_httpHandler;

void on_initialize(const utility::string_t& address)
{
    web::uri_builder uri(address);

    auto addr = uri.to_uri().to_string();
    g_httpHandler = std::unique_ptr<http_handler::HttpHandler>(new http_handler::HttpHandler(addr));
    g_httpHandler->open().wait();

    ucout << utility::string_t(U("Listening for requests at: ")) << addr << std::endl;
}

void on_shutdown()
{
    g_httpHandler->close().wait();
}

int main(int argc, char* argv[])
{
    Magick::InitializeMagick(*argv);

    utility::string_t port = U("8080");
    if (argc == 2)
    {
        port = argv[1];
    }

    utility::string_t address = U("http://127.0.0.1:");
    address.append(port);

    on_initialize(address);
    std::cout << "Press ENTER to exit." << std::endl;

    std::string line;
    std::getline(std::cin, line);

    on_shutdown();

    return 0;
}