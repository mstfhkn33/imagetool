#pragma once

#include <Magick++.h>

#include <string>

namespace image_magick
{
class IMagickWrapper
{
  public:
    IMagickWrapper(std::string inFile);
    ~IMagickWrapper() = default;

    bool resizeImage(std::string newSize, std::string outFile);
    bool rotateImage(std::string rotateDegree, std::string outFile);
    bool cropImage(std::string cropOpt, std::string outFile);
    bool grayScaleImage(std::string outFile);

  private:
    Magick::Image inImage;
    bool isInit;
};
}  // namespace image_magick