#include "IMagickWrapper.hpp"

namespace image_magick
{
std::string getCropToken(std::string& s, std::string delim = " ")
{
    std::string token;
    if (s.back() != '\0')
    {
        auto parsed_till = s.find_last_of('\0') + 1, token_end = s.find(delim, parsed_till);
        if (token_end != std::string::npos)
        {
            token = s.substr(parsed_till, token_end - parsed_till);
            s.replace(token_end, delim.size(), delim.size(), '\0');
        }
        else
        {
            token = s.substr(parsed_till);
            s += '\0';
        }
    }
    return token;
}

IMagickWrapper::IMagickWrapper(std::string inFile)
{
    isInit = true;
    try
    {
        // Read a file into image object
        inImage.read(inFile);
    }
    catch (Magick::Exception& error_)
    {
        isInit = false;
    }
}
bool IMagickWrapper::resizeImage(std::string newSize, std::string outFile)
{
    if (!isInit)
    {
        return false;
    }
    try
    {
        Magick::Image resizedImage = inImage;
        resizedImage.resize(newSize + "!");
        resizedImage.write(outFile);
    }
    catch (Magick::Exception& error_)
    {
        return false;
    }
    return true;
}

bool IMagickWrapper::rotateImage(std::string rotateDegree, std::string outFile)
{
    if (!isInit)
    {
        return false;
    }
    try
    {
        Magick::Image resizedImage = inImage;
        resizedImage.rotate(std::stod(rotateDegree));
        resizedImage.write(outFile);
    }
    catch (Magick::Exception& error_)
    {
        return false;
    }
    return true;
}

bool IMagickWrapper::cropImage(std::string cropOpt, std::string outFile)
{
    if (!isInit)
    {
        return false;
    }
    unsigned int width, height, xOffset, yOffset;
    std::string token = getCropToken(cropOpt, ",");
    width = std::stoi(token);
    token = getCropToken(cropOpt, ",");
    height = std::stoi(token);
    token = getCropToken(cropOpt, ",");
    xOffset = std::stoi(token);
    token = getCropToken(cropOpt, ",");
    yOffset = std::stoi(token);
    try
    {
        Magick::Image resizedImage = inImage;
        resizedImage.crop(Magick::Geometry(width, height, xOffset, yOffset));
        resizedImage.write(outFile);
    }
    catch (Magick::Exception& error_)
    {
        return false;
    }
    return true;
}

bool IMagickWrapper::grayScaleImage(std::string outFile)
{
    if (!isInit)
    {
        return false;
    }
    try
    {
        Magick::Image resizedImage = inImage;
        resizedImage.type(Magick::GrayscaleType);
        resizedImage.write(outFile);
    }
    catch (Magick::Exception& error_)
    {
        return false;
    }
    return true;
}

}  // namespace image_magick