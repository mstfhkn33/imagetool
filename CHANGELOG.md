# CHANGELOG

## Unversioned Changes

- 

## v1.1.2

- Add prerequisites in README file.

## v1.1.1

- Add usage in README file.

## v1.1.0

- Add missing part from initial repository.
- Add URL parser to parse the given HTTP GET request to be able to get variables.
- Add curl wrapper to be able to download the given image url to local.
- Add Image Magick wrapper to be able to do operations on the image.
- Return image file in http_response(cpprestsdk) to give the result to the user.

## v1.0.0

- Initial cpp project is created with cmake and build script.
