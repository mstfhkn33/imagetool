#!/bin/bash

usage() {
    echo "usage"
}

do_gen_project() {
    mkdir -p build
    pushd build
        cmake ..
    popd
}

build() {
    do_gen_project
    pushd build
        make -j $(nproc)
    popd
    exit 0
}

clean() {
    rm -rf build
    exit 0
}

while getopts "h:" arg; do
  case $arg in
    h)
      usage 
      ;;
  esac
done

for i in "$@"; do
  case $i in
    build)
      build
      shift # past argument=value
      ;;
    clean)
      clean
      shift # past argument=value
      ;;
    *)
      echo "Unknown operation $i"
      exit 1
      ;;
  esac
done

exit 0
