## History

Refer to CHANGELOG.md

## Image Tool

This tool will be able to start a web server and accept some parameter to resize the given image.

### Prerequisites

- cpprestsdk
- libcurl
- ImageMagick & Magick++

### Usage

Run the build/imagetool binary file and try to use this urls in any web browser:

URLs:

- http://localhost:8080/v1/resize:640x360/url:upload.wikimedia.org/wikipedia/en/a/a9/Example.jpg
- http://localhost:8080/v1/rotate:90/url:upload.wikimedia.org/wikipedia/en/a/a9/Example.jpg
- http://localhost:8080/v1/crop:200,200,0,0/url:upload.wikimedia.org/wikipedia/en/a/a9/Example.jpg
- http://localhost:8080/v1/grayscale/url:upload.wikimedia.org/wikipedia/en/a/a9/Example.jpg

